import * as actionTypes from '../constants/actionTypes'

export function showPlayer() {
  return {
    type: actionTypes.SHOW_PLAYER,
    playerIsVisible: true
  }
}

export function hidePlayer() {
  return {
    type: actionTypes.HIDE_PLAYER,
    playerIsVisible: false
  }
}
