import { setTracks } from './tracks'
import { playTrack, setCurrentTrack, showTrackDetails, hideTrackDetails } from './track'
import { showPlayer, hidePlayer } from './player'

export {
  setTracks,
  playTrack,
  setCurrentTrack,
  showPlayer,
  hidePlayer,
  showTrackDetails,
  hideTrackDetails
}
