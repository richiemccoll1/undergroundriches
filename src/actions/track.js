import * as actionTypes from '../constants/actionTypes'
import API from '../api/api'

function requestTrack() {
  return { type: actionTypes.REQUEST_TRACK }
}

function receiveTrack(response = {}) {
  return { type: actionTypes.RECEIVE_TRACK, info: response }
}

function receiveTrackError() {
 return { type: actionTypes.RECEIVE_TRACK_ERROR, error: true }
}

export function playTrack(track) {
  return (dispatch) => {
    dispatch(requestTrack())
    API.resolveTrack(track)
    .done((data) => {
      dispatch(receiveTrack(data))
    })
    .fail(() => {
      dispatch(receiveTrackError())
    })
  }
}

export function setCurrentTrack(track) {
  return {
    type: actionTypes.SET_CURRENT_TRACK,
    track
  }
}

export function showTrackDetails() {
  return {
    type: actionTypes.SHOW_TRACK_DETAILS,
    isShowingInfo: true
  }
}

export function hideTrackDetails() {
  return {
    type: actionTypes.HIDE_TRACK_DETAILS,
    isShowingInfo: false
  }
}
