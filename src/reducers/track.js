import * as actionTypes from '../constants/actionTypes'

const initialState = {}

export default function(state = initialState, action) {
  switch(action.type) {
    case actionTypes.REQUEST_TRACK:
      return { ...state, loading: true }
    case actionTypes.RECEIVE_TRACK:
      return { ...state, trackInfo: action.info }
    case actionTypes.RECEIVE_TRACK_ERROR:
      return { ...state, errorGettingTrack: true }  
    case actionTypes.SET_CURRENT_TRACK:
      return { ...state, currentTrack: action }
    case actionTypes.SHOW_TRACK_DETAILS:
      return { ...state, isShowingInfo: action.isShowingInfo }
    case actionTypes.HIDE_TRACK_DETAILS:
      return { ...state, isShowingInfo: action.isShowingInfo }
    default:
      return state
  }
}
