import * as actionTypes from '../constants/actionTypes'

const initialState = {
  playerIsVisible: false
}

export default function(state = initialState, action) {
  switch(action.type) {
    case actionTypes.SHOW_PLAYER:
      return showPlayer(state, action)
    case actionTypes.HIDE_PLAYER:
      return hidePlayer(state, action)
    default:
      return state
  }
}

function showPlayer(state, action) {
  const { playerIsVisible } = action
  return { ...state, playerIsVisible }
}

function hidePlayer(state, action) {
  const { playerIsVisible } = action
  return { ...state, playerIsVisible }
}
