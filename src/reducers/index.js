import { combineReducers } from 'redux'
import tracks from './tracks'
import track from './track'
import player from './player'

export default combineReducers({
  tracks,
  track,
  player
})
