import React from 'react'

const ProgressBar = (props) => {

  let time = Math.floor(props.seek / 60) + ": " + props.seek.toFixed()

  return (
    <div className="Player-TrackProgress">
    <div className="Player-TrackProgress-CurrentTime">{time}</div>
      <progress value={props.seek.toFixed(2)} max={props.duration}>{props.seek.toFixed(2)}</progress>
      <div className="Player-TrackProgress-Time">{props.duration.toFixed()}</div>
    </div>
  )
}

export default ProgressBar
