import React from 'react'
import Player from './Player'

class PlayerContainer extends React.Component {

  constructor() {
    super()
  }

  render() {
    return (
      <Player track={this.props.track} hidePlayer={this.props.hidePlayer} />
    )
  }
}

export default PlayerContainer
