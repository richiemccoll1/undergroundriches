import React from 'react'
import ReactHowler from 'react-howler'
import Loading from 'react-loading'
import raf from 'raf'
import ProgressBar from './ProgressBar'

class Player extends React.Component {
    constructor() {
      super()
      this.closePlayer = this.closePlayer.bind(this)
      this.handlePlay = this.handlePlay.bind(this)
      this.handlePause = this.handlePause.bind(this)
      this.handleOnEnd = this.handleOnEnd.bind(this)
      this.handleOnLoad = this.handleOnLoad.bind(this)
      this.renderSeekPos = this.renderSeekPos.bind(this)
      this.state = {
        volume: 1,
        playing: true,
        loaded: false,
        siriWave: null
      }
    }

    renderSeekPos () {
      this.setState({
        seek: this.player.seek()
      })
      if (this.state.playing) {
        this._raf = raf(this.renderSeekPos)
      }
    }

    handlePlay () {
      this.setState({ playing: true })
      this.renderSeekPos()
    }

    handlePause () {
      this.setState({ playing: false })
      this.renderSeekPos()
    }

    handleOnEnd () {
      this.setState({ playing: false })
      this.clearRAF()
    }

    handleOnLoad () {
     this.setState({
       loaded: true,
       duration: this.player.duration()
     })
     this.renderSeekPos()
   }

    closePlayer() {
      this.props.hidePlayer()
    }

    clearRAF () {
      raf.cancel(this._raf)
    }

    componentWillUpdate(nextProps, nextState) {
      return (
        this.props.track.currentTrack !== nextProps.track.currentTrack
      )
    }

    componentWillUnmount () {
      this.clearRAF()
    }

    render() {
      const { title } = this.props.track.currentTrack.track
      const { errorGettingTrack } = this.props.track

      return (
        <div className="Player">
          {errorGettingTrack &&
            <span>Oops something went wrong fetching the track. </span>
          }

          {!this.state.loaded && !errorGettingTrack &&
            <div className="Player-LoadingTrack">
              <Loading type='bars' color='#000000' width="30" height="30" />
            </div>
          }

          {this.state.seek &&
            <div>
              <ProgressBar seek={this.state.seek} duration={this.state.duration} />
            </div>
          }

          <ReactHowler
            src='https://ia902606.us.archive.org/35/items/shortpoetry_047_librivox/song_cjrg_teasdale_64kb.mp3'
            playing={this.state.playing}
            volume={this.state.volume}
            onEnd={this.handleOnEnd}
            onLoad={this.handleOnLoad}
            onPlay={this.handleOnPlay}
            ref={(ref) => (this.player = ref)}
          />

          {this.state.playing
            ? <i className="material-icons" onClick={this.handlePause}>pause</i>
            : <i className="material-icons" onClick={this.handlePlay}>play_arrow</i>
          }
          <div className="Player-VolumeControl">
          <label>
            <input
              type='range'
              min='0'
              max='1'
              step='.05'
              value={this.state.volume}
              onChange={e => this.setState({volume: parseFloat(e.target.value)})}
            />
          </label>
          </div>
          <div className="Player-Close">
            <i className="material-icons" onClick={this.closePlayer}>close</i>
          </div>
        </div>
      )
    }
}

export default Player
