import React from 'react'

class Track extends React.Component {
  constructor() {
    super()
    this.playTrack = this.playTrack.bind(this)
    this.pauseTrack = this.pauseTrack.bind(this)
    this.showTrackInfo = this.showTrackInfo.bind(this)
    this.isShowingInfo = false
    this.state = {
      isPlaying: false
    }
  }

  showTrackInfo = () => {
    if(this.isShowingInfo === false) {
      this.props.showTrackDetails()
      this.isShowingInfo = true
    } else {
      this.props.hideTrackDetails()
      this.isShowingInfo = false
    }
  }

  playTrack = () => {
    this.props.playTrack(this.props.trackLink)
    this.props.setCurrentTrack(this.props)
    this.props.showPlayer()
  }

  pauseTrack = () => {
    this.setState({ isPlaying: false })
    console.log('pause track')
  }

  render() {

    const { title, artist, artwork, facebookLink, soundcloudLink, tag } = this.props

    let isPlaying
    if (this.props.currentTrack === this.props.title) {
      isPlaying = <i className="material-icons Track-Pause" onClick={this.pauseTrack}>pause</i>
    } else {
      isPlaying = <i className="material-icons Track-Play" onClick={this.playTrack}>play_arrow</i>
    }

    let backgroundImage = { backgroundImage: artwork }

    return (
        <div className="Track">
          <div className="Track-Artwork" style={backgroundImage}></div>
          <div className="Track-Details-isPlaying">  {isPlaying} </div>
          <summary className="Track-Details">
            <div className="Track-Details-Song">
              <h1>{title}</h1>
              </div>
          </summary>
        </div>
    )
  }
}

export default Track
