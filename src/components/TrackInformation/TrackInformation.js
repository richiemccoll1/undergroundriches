import React from 'react'

class TrackInformation extends React.Component {
  constructor() {
    super()
    this.closeTrackInformation = this.closeTrackInformation.bind(this)
  }

  closeTrackInformation = () => {
    this.props.hideTrackDetails()
  }

  render() {
    return (
      <div className="overlay">
      <div className="TrackInformation">
        <div className="TrackInformation-Close">
          <i className="material-icons" onClick={this.closeTrackInformation}>close</i>
        </div>
        Hello World
      </div>
    </div>
    )
  }

}
export default TrackInformation
