import React, { Component } from 'react';
import Track from '../Track/Track'
import TrackInformation from '../TrackInformation/TrackInformation'
import {Col} from 'react-bootstrap'
import Nav from '../Nav/Nav'
import PlayerContainer from '../Player/PlayerContainer'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../../actions'
import SocialLinks from '../SocialLinks/SocialLinks'

class App extends Component {
  render() {

    const { tracks, track, playTrack, setCurrentTrack, hidePlayer, showPlayer, player, showTrackDetails, hideTrackDetails } = this.props
    const { trackName, isShowingInfo } = track
    const { playerIsVisible } = player


    return (
      <div className={isShowingInfo ? "App-Overlay" : "App"}>
        <Nav title={"Underground Riches"} />
          {isShowingInfo &&
              <TrackInformation hideTrackDetails={hideTrackDetails} />
          }
          <SocialLinks />
          <div className="container">

            {tracks.map((track, key) => {
              return <Col xs={12} md={4} sm={6} key={key}>
                        <Track key={track.id}
                               title={track.trackTitle}
                               trackLink={track.trackURL}
                               artist={track.trackArtist}
                               playTrack={playTrack}
                               setCurrentTrack={setCurrentTrack}
                               currentTrack={trackName}
                               showPlayer={showPlayer}
                               artwork={track.trackArtwork}
                               soundcloudLink={track.artistSoundcloud}
                               facebookLink={track.artistFacebook}
                               showTrackDetails={showTrackDetails}
                               hideTrackDetails={hideTrackDetails}
                               tag={track.tag}
                               />
                    </Col>
            })}
         </div>
         {playerIsVisible ? <PlayerContainer track={track} hidePlayer={hidePlayer} /> : ""}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const tracks = state.tracks
  const track = state.track
  const player = state.player
  return {
    tracks,
    track,
    player
  }
}

function mapDispatchToProps(dispatch) {
  return {
    playTrack: bindActionCreators(actions.playTrack, dispatch),
    setCurrentTrack: bindActionCreators(actions.setCurrentTrack, dispatch),
    showPlayer: bindActionCreators(actions.showPlayer, dispatch),
    hidePlayer: bindActionCreators(actions.hidePlayer, dispatch),
    showTrackDetails: bindActionCreators(actions.showTrackDetails, dispatch),
    hideTrackDetails: bindActionCreators(actions.hideTrackDetails, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
