import React from 'react'
import soundcloudIcon from '../../images/soundcloudIcon.svg'
import fbIcon from '../../images/fbIcon.svg'
import instagramIcon from '../../images/instagramIcon.svg'

const SocialLinksStyle = {
  display: 'flex',
  justifyContent: 'center',
  marginBottom: '2em'
}

const SocialIconStyle = {
    height: '2.5em',
    marginRight: '0.6em'
}

const SocialLinks = () => {
  return (
    <div style={SocialLinksStyle}>
      <a href="https://soundcloud.com/underground-riches" target="_blank">
        <img src={soundcloudIcon} style={SocialIconStyle} alt="Underground Riches Soundcloud" />
       </a>
       <a href="https://www.facebook.com/undergroundriches/" target="_blank">
        <img src={fbIcon} style={SocialIconStyle} alt="Underground Riches Facebook" />
       </a>
       <a href="https://www.instagram.com/undergroundrichesmusic/" target="_blank">
        <img src={instagramIcon} style={SocialIconStyle} alt="Underground Riches Instagram" />
       </a>
    </div>
  )
}

export default SocialLinks
