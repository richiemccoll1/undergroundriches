import React from 'react'

const Nav = (props) => {
  return (
    <div className="nav">
      <div className="nav-title">
        {props.title}
      </div>
    </div>
  )
}

export default Nav
