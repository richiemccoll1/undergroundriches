const TracksData = [
  {
    id: 1,
    trackTitle: 'Do You Want My Love',
    trackURL: 'https://www.youtube.com/watch?v=J1SWn2IGOV4',
    trackArtwork: 'url(https://scontent.cdninstagram.com/t51.2885-15/s480x480/e35/16122913_224814634647123_8081564902587629568_n.jpg?ig_cache_key=MTQzNzMxOTQ3MjcyNTQxMjIyNg%3D%3D.2)'
  },
  {
    id: 2,
    trackTitle: 'Hyperfunk',
    trackURL: 'https://www.youtube.com/watch?v=PB3yQwJqNW0',
    trackArtwork: 'url(https://scontent.cdninstagram.com/t51.2885-15/s480x480/e35/16228851_238152616638588_8962360083933560832_n.jpg?ig_cache_key=MTQzNDcyMzIwOTUwNDk1NTcyMQ%3D%3D.2)'
  },
  {
    id: 3,
    trackTitle: 'Dræm Girl',
    trackURL: 'https://www.youtube.com/watch?v=_ODKdStnIuM',
    trackArtwork: 'url(https://scontent.cdninstagram.com/t51.2885-15/s480x480/e35/16228598_891845037584678_6575345688928845824_n.jpg?ig_cache_key=MTQzNDcyNTIxNDE5Nzg1NDMzMg%3D%3D.2)'
  }
]

export default TracksData
