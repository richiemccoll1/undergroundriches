import $ from 'jquery'

// soundcloud API
// const baseURL = 'https://api.soundcloud.com'
// const clientID = 'a3b90f1c9532ce48f0de95aee25adc91'

//youtube to mp3
const baseURL = 'http://www.youtubeinmp3.com/fetch/?format=JSON&video='

class API {

  static resolveTrack(url) {
    // soundcloud API
    // let resolveUrl = baseURL + '/resolve.json?url=' + encodeURIComponent(url) + '&client_id=' + clientID
    console.log(url)
    let resolveUrl = baseURL + url
    return API.getJSON(resolveUrl)
  }

    static getJSON(url) {
      return $.getJSON(url)
    }

    static doRequest(method, url, data) {
      return $.ajax({
      method: method,
      url: url,
      data: data,
      dataType: "json"
    })
  }
}

export default API
