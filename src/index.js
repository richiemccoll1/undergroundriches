import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import './index.css';
import configureStore from './stores/configureStore'
import * as actions from './actions'
import { Provider } from 'react-redux'
import TracksData from './data/TracksData'

const store = configureStore()
store.dispatch(actions.setTracks(TracksData))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
