# undergroundriches

# TO DO

- Consolidate CSS into LESS. Better structure.
- Finish the markup for the player.
- Optimize the streaming functionality.
- Consolidate all application state into redux. Unified state for track, player etc.
